#!/usr/bin/env octave

1;

function class_name = parse_class_name (fpath, filename_format)
  [dir, fname, ext] = fileparts (fpath);
  [class_name, ~, err] = sscanf ([fname ext], filename_format);
  if (! isempty (err))
    error("failed to scan class from results filename '%s': %s", fpath, err);
  endif
endfunction

function [rv] = main (argv)
  if (length (argv) != 1)
    error("We only expect one command line argument, got %s", argv);
  endif
  results_fpath = argv{1};

  run ("VOCinit");

  [results_dir, class_name, ~] = fileparts (results_fpath);
  figure_path = fullfile (results_dir, [class_name "-pr-curve.png"]);

  if (! any (strcmp (class_name, VOCopts.classes)))
    error ("no class named '%s'", class_name);
  endif

  ## By default, VOC expects results in a directory relative to the
  ## VOC code directory but we keep the code on srcdir and the results
  ## on builddir so we need to change that.  In addition, we also
  ## change the results filename format.  The VOC default filename
  ## format is "%s_cls_val_%s.txt".  The first "%s" is an identifier
  ## for the results (I guess it makes sense when having multiple
  ## results on the same directory with different identifiers for each
  ## classifier but we are using this for out of source testing so
  ## it's pointless).  The second "%s" is the class name.  This
  ## clsrespath will be used internally by VOCevalcls.
  VOCopts.clsrespath = fullfile (results_dir, "%s%s.txt");

  fh = figure ("visible", false);

  ## textread and strmatch are obsolete but used in the VOC code
  warning ("off", "Octave:legacy-function");
  [rec, prec, ap] = VOCevalcls (VOCopts, "", class_name, true);

  print (figure_path);

  printf ("%.3f\n", ap);

  rv = 0;
endfunction

main(argv ());
