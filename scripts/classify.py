#!/usr/bin/env python3

import argparse
import sys
import os.path
import threading
import time

from pyclient.visor_client import VisorClient
from pyclient.visor_notifier import VisorNotifier
import pyclient.cpuvisor_srv_pb2 as cpuvisor_srv

# This file is located at (relative to the VOC devkit):
#
#     root of git repo
#     ├── ...
#     ├── scripts
#     │   ├── ...
#     │   └── runtest.py (this script)
#     ├── ...
#     └── VOCdevkit
#         ├── ...
#         └── VOC2012
#             ├── ImageSets
#             ├── JPEGImages

root_dir = os.path.dirname(os.path.dirname(__file__))
devkit_dir = os.path.join(root_dir, "VOCdevkit")
voc2012_dir = os.path.join(devkit_dir, "VOC2012")


class ProcessedImagesCounter:
    def __init__(self, config_fpath, query):
        self._query = query
        self._count = 0
        self._lock = threading.Lock()
        self._notifier = VisorNotifier(config_fpath, self.process_notification)

    @property
    def count(self):
        return self._count

    def process_notification(self, notification):
        if (notification.id == self._query
            and notification.type == cpuvisor_srv.NTFY_IMAGE_PROCESSED):
            with self._lock:
                self._count += 1


def get_training_images(class_name):
    train_txt = os.path.join(voc2012_dir, "ImageSets", "Main",
                             class_name + "_train.txt")

    image_ids = []
    with open(train_txt, "r") as fh:
        for line in fh:
            img_id, val = line.split()
            if val == "1":  # XXX: does not include difficult images (0)
                image_ids.append(img_id)

    base_dir = os.path.abspath(os.path.join(voc2012_dir, "JPEGImages"))
    return [os.path.join(base_dir, iid + ".jpg") for iid in image_ids]


def test_class(config_fpath, class_name, limit):
    client = VisorClient(config_fpath)
    query = client.start_query()
    processed_images_counter = ProcessedImagesCounter(config_fpath, query)
    training_images = get_training_images(class_name)[:limit]
    for image_path in training_images:
        client.add_trs_from_file(query, image_path)

    while processed_images_counter.count != len(training_images):
        time.sleep(1)

    # FIXME: we rely on having page_size of 10000 otherwise which
    # is higher than the number of images.
    ranking = client.train_rank_get_ranking(query)
    client.free_query(query)
    # convert to tuple and remove file extension
    ranking = [(r.path[:-4], r.score) for r in ranking.rlist]
    ranking.sort(key=lambda x : x[0])
    for r in ranking:
        print(r[0], r[1])


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--training-limited-to", dest="limit", type=int,
                        default=0, help="Limit training to N images")
    parser.add_argument("config_fpath", type=str,
                        help='Path for cpuvisor server configuration')
    parser.add_argument("class_name", type=str,
                        help='Name of class to measure performance.')
    args = parser.parse_args(argv[1:])
    test_class(args.config_fpath, args.class_name, limit=args.limit)


if __name__ == "__main__":
    main(sys.argv)
